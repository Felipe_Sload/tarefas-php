<h1>Tarefa: <?php echo $tarefa['nome']; ?></h1>

<p>
	<strong>Conclúida: </strong>
	<?php echo traduz_concluida($tarefa['concluida']); ?>
</p>
<p>
	<strong>Descrição:</strong>
	<?php echo nl2br($tarefa['descricao']); ?>
</p>
<p>
	<strong>Prazo:</strong>
	<?php echo traduz_data_para_tabela($tarefa['prazo']); ?>
</p>

<?php if (count($anexo) > 0 ): ?>
	<p>
		<strong>Atenção! </strong> Esta tarefa contém anexos!
	</p>
<?php endif ?>	