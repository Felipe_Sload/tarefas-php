<div class="col-md-4">
	<form method="POST">
		<div class="form-group">
			<input type="hidden" name='id' class='form-group' value="<?php echo $tarefa['id']; ?>" >
			<label for="taref">Tarefa
			<?php if ($tem_erros && isset($erros_validacao['nome'])): ?>
				<span class='text-danger'>
					<?php echo $erros_validacao['nome']; ?>
			<?php endif; ?>
				</span>
			</label>
			<input type="text" class="form-control" id="taref" name="nome" value="<?php echo $tarefa['nome']; ?>" >
			<label for="desc">Descrição (Opcional):
			</label>
			<textarea id="desc" class="form-control" name="descricao"><?php echo $tarefa['descricao']; ?></textarea>
			<label for="praz">Prazo (Opcional):
			<?php if ($tem_erros && isset($erros_validacao['prazo'])): ?>
			 	<span class='text-danger'>
			 		<?php echo $erros_validacao['prazo']; ?>
			 	
			 <?php endif ?>
			 	</span> </label>
			<input type="text" id="praz" class="form-control" name="prazo" value="<?php echo traduz_data_para_tabela($tarefa['prazo']); ?>"><br>										
		<div class="radio-inline">
			<label class="radio-inline" >
			<input type="radio" name="prioridade" value="3" <?php echo ($tarefa['prioridade'] ==3)? 'checked':''; ?>>baixa											
			</label>
		</div>
		<div class="radio-inline">
			<label class="radio-inline" for="med">
			<input type="radio" name="prioridade" value="2" <?php echo ($tarefa['prioridade'] ==2)? 'checked':''; ?> >média 
			</label>
		</div>	
		<div class="radio-inline">
			<label class="radio-inline" >
			<input type="radio" name="prioridade" value="1"  <?php echo ($tarefa['prioridade'] ==1)? 'checked':''; ?>>alta 
			</label>
		</div>	
			<br>
		<div class="checkbox">	
			<label>
			<input type="checkbox" name="concluida" value="1"  <?php echo ($tarefa['concluida'] ==1)? 'checked': ''; ?>>Tarefa Concluída:</label>
		</div>
		<div class="checkbox">
			<label>
				<input type="checkbox" name="lembrete" value="1" ?>Lembrete por e-mail:
			</label>
		</div>
			<br>
			<input type="submit" class="btn btn-primary" value="<?php echo ($tarefa['id'] > 0 )?'Atualizar':'Cadastrar'; ?>">
		<?php if ($tarefa['id']>0): ?>
			<a class="btn btn-danger" href='tarefas.php'>Cancelar</a>
		<?php endif ?>
		</div>
	</form>
</div>