				<div class="col-md-8">
							<table class="table table-striped">
								<tr>
									<th>Nome</th>
									<th>Descrição</th>
									<th>Prazo</th>
									<th>Prioridade</th>
									<th>Concluída</th>
									<th>Opções</th>
								</tr>
								
								<?php foreach ($lista_tarefas as $tarefa): ?>
										<tr>
											<td><a href="tarefa.php?id=<?php echo $tarefa['id']; ?>"><?php echo $tarefa['nome']; ?></a></td>
											<td><?php echo $tarefa['descricao']; ?></td>
											<td><?php echo traduz_data_para_tabela($tarefa['prazo']); ?></td>
											<td><?php echo traduz_prioridade($tarefa['prioridade']);	?></td>
											<td><?php echo traduz_check($tarefa['concluida']); ?></td>
											<td>
											<div class='btn-group-vertical' role='group' arial-label='...'>
												<a type='button' class='btn btn-info btn-sm' href='editar.php?id=<?php echo $tarefa['id']; ?>'>Editar</a>
												<a type='button' class='btn btn-danger btn-sm' href='remover.php?id=<?php echo $tarefa['id']; ?>'>Remover</a>
												<a type='button' class="btn btn-success btn-sm" href='duplicar.php?id=<?php echo $tarefa['id']; ?>'>Duplicar</a>
											</div>
											</td>
										</tr>
								<?php endforeach ?>	
									<tr><th><a class='btn btn-danger' href="removerTodos.php?">Apagar Tarefas Concluídas</a></th> </tr>			
							</table>
					</div>