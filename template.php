<html>
<head>
	<meta charset="utf-8">
	<title>Lista de Tarefas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" src="//normalize-css.googlecode.com/svn/trunk/normalize.css" />
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">

</head>
<body>
	<div class="panel panel-primary">
		<div class="panel-heading"><h1>Lista de Tarefas</h1></div>	
			<div class="panel-body">
				<div class="row-fluid">
				<?php include 'formulario.php'; ?>
			
				<?php if ($exibir_tabela): ?>
					<?php include 'tabela.php' ?>
				<?php endif ?>

				</div>	
			</div>	
		</div>
	
</body>
</html>